<?php
 
namespace App\Http\Controllers\Api;
 
use App\Models\User;
use App\Mail\sendEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
 
class UserController extends Controller
{
    /**
     * Create User
     * @param Request $request
     * @return User
     */
    public function createUser(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(),
            [
                'username' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'groupuser' => 'required'
            ]);
 
            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
 
            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                $userakses = User::where('id', $userid)->first();
                $username = $userakses->user_name;
            }
 
            $user = User::create([
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'user_name' => $request->username,
                // 'id_card' => $request->id_card,
                // 'npwp' => $request->npwp,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'sex' => $request->sex,
                'department' => $request->department,
                'company' => $request->company,
                'phone' => $request->phone,
                'sex' => $request->sex,
                'image' => $request->image,
                'province' => $request->province,
                'city' => $request->city,
                'address' => $request->address,
                'status' => 0,
                // 'create_by' => $request->create_by,
                'create_by' => $username,
                // 'modified_by' => $request->modified_by
                'modified_by' => $username,
                'group_users_id' => $request->groupuser
            ]);
 
            return response()->json([
                'status' => true,
                'message' => 'User Created Successfully',
                'token' => $user->createToken("API TOKEN")->plainTextToken
            ], 200);
 
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
 
    /**
     * Login The User
     * @param Request $request
     * @return User
     */
    public function loginUser(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(),
            [
                // 'email' => 'required|email',
                'user_name' => 'required',
                'password' => 'required'
            ]);
 
            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
 
            // if(!Auth::attempt($request->only(['email', 'password']))){
            //     return response()->json([
            //         'status' => false,
            //         'message' => 'Email & Password does not match with our record.',
            //     ], 401);
            // }
 
            $users = DB::table('users')
            ->select('users.group_users_id')
            ->where('users.user_name', $request->user_name)
            ->get();
           
            // dd(count($users));
            if(count($users)>0){
                $grupusers=$users[0]->group_users_id;
            }else{
                $grupusers=2;
            }
 
            // dd($grupusers);
 
            if($grupusers == 2){
 
                return response()->json([
                    'status' => false,
                    'message' => 'Username & Password does not match with our authentication.',
                ], 401);
            }
 
            if(!Auth::attempt($request->only(['user_name', 'password']))){
                return response()->json([
                    'status' => false,
                    'message' => 'Username & Password does not match with our record.',
                ], 401);
            }
 
            // $user = User::where('email', $request->email)->first();
            $user = User::where('user_name', $request->user_name)->first();
 
            return response()->json([
                'status' => true,
                'message' => 'User Logged In Successfully',
                'token' => $user->createToken("API TOKEN")->plainTextToken
            ], 200);
 
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
   
    public function CountUser(Request $request)
    {
        try {
            $jumUser = DB::table('users')
                    ->leftjoin('group_users', 'users.group_users_id', '=', 'group_users.id')
                    ->select('users.*', 'group_users.group_name')
                    ->where('users.deleted_by','=',null)
                    ->count();
 
            return response()->json([
                'jumUser' => $jumUser
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
   
    public function ViewUser(Request $request)
    {
        $post_data = $request->all();
        $page = $post_data['page'];
        $rowpage = $post_data['rowpage'];
        $ordby = $post_data['ordby'];
        $ordasc = $post_data['ordasc'];
 
        $jumUser = DB::table('users')
        ->leftjoin('group_users', 'users.group_users_id', '=', 'group_users.id')
        ->select('users.*', 'group_users.group_name')
        ->where('users.deleted_by','=',null)
        ->count();
 
        $jumdata = $jumUser;
        $jumpage = CEIL($jumUser/$rowpage);
        $start = ($page - 1) * $rowpage;
        try {
            $user = DB::table('users')
                    ->leftjoin('group_users', 'users.group_users_id', '=', 'group_users.id')
                    ->select('users.*', 'group_users.group_name')
                    ->where('users.deleted_by','=',null)
                    ->skip($start)
                    ->take($rowpage)
                    ->orderBy($ordby,$ordasc)                    
                    ->get();
 
            return response()->json([
                'data' => $user,
                'jumData' => $jumdata,
                'jumPage' => $jumpage
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
   
    public function searchUser(Request $request)
    {
        $post_data = $request->all();
        try {
            // $user = DB::table('users')
            //         ->leftjoin('group_users', 'users.group_users_id', '=', 'group_users.id')
            //         ->select('users.*', 'group_users.group_name')
            //         ->where(function ($query) use ($post_data){
            //             $query->where('users.user_name','like', '%'. $post_data['search'] .'%')
            //             ->orWhere('users.firstname','like', '%'. $post_data['search'] .'%')
            //             ->orWhere('users.lastname','like', '%'. $post_data['search'] .'%')
            //             ->orWhere('users.email','like', '%'. $post_data['search'] .'%')
            //             ->orWhere('group_users.group_name','like', '%'. $post_data['search'] .'%');
            //         })
            //         ->where('users.deleted_by','=',null)
            //         ->get();
 
            $user = DB::select("select users.*, group_users.group_name from users
                                    left join group_users on users.group_users_id=group_users.id
                                    where users.deleted_by is null
                                    and (LOWER(group_users.group_name) like '%".strtolower($post_data['search'])."%'
                                        or LOWER(users.user_name) like '%".strtolower($post_data['search'])."%'
                                        or LOWER(users.firstname) like '%".strtolower($post_data['search'])."%'
                                        or LOWER(users.lastname) like '%".strtolower($post_data['search'])."%'
                                        or LOWER(users.email) like '%".strtolower($post_data['search'])."%')");
            return response()->json([
                'data' => $user
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
   
    public function DeleteUser(Request $request)
    {
        $datenow = date('Y-m-d H:i:s');
        $post_data = $request->all();
        if (isset($post_data['token'])) {
            [$id, $user_token] = explode('|', $post_data['token'], 2);
            $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
            $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
            $userakses = User::where('id', $userid)->first();
            $username = $userakses->user_name;
        }
 
        try {
            $user = User::where('id', $request->iduser)
            ->update([
                'deleted_by' => $username,
                'deleted_at' => $datenow
            ]);
 
            return response()->json([
                'status' => true,
                'message' => 'User Deleted Successfully'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' =>  $userakses,
                'message' => $th->getMessage()
            ], 500);
        }
    }
   
    public function updateUser(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(),
            [
                'username' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'groupuser' => 'required'
            ]);
 
            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
 
            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                $userakses = User::where('id', $userid)->first();
                $username = $userakses->user_name;
            }
 
            User::where('id', $request->id)
            ->update([
                'email' => $request->email,
                'user_name' => $request->username,
                // 'id_card' => $request->id_card,
                // 'npwp' => $request->npwp,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'sex' => $request->sex,
                'department' => $request->department,
                'company' => $request->company,
                'phone' => $request->phone,
                'image' => $request->image,
                'province' => $request->province,
                'city' => $request->city,
                'address' => $request->address,
                'status' => 0,
                'modified_by' => $username,
                'group_users_id' => $request->groupuser
            ]);
 
            return response()->json([
                'status' => true,
                'message' => 'User Updated Successfully'
            ], 200);
 
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
   
    public function menuUser(Request $request)
    {
        try {
            //Validated    
            $userid=0;  
            $post_data = $request->all();
            if (isset($post_data['usToken'])) {
                [$id, $user_token] = explode('|', $post_data['usToken'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
            }    
 
            $GroupMenu = DB::table('group_menus')
                    // ->leftJoin('group_menus', 'menus.menu_id', '=', 'group_menus.menu_id', 'and', 'group_menus.group_users_id', '=', $request->group_id)
                    ->join('menus', 'menus.menu_id', '=', 'group_menus.menu_id')
                    ->join('users', 'users.group_users_id', '=', 'group_menus.group_users_id')
                    ->select('menus.*', 'group_menus.group_users_id', 'group_menus.mview',
                                'group_menus.madd', 'group_menus.medit', 'group_menus.mdelete',
                                'group_menus.mapprove', 'group_menus.mignore', 'group_menus.mexport')
                    ->where('users.id','=',$userid)
                    ->orderBy('menus.menu_seq')
                    ->get();
 
            $user = DB::table('users')
                    ->leftjoin('group_users', 'users.group_users_id', '=', 'group_users.id')
                    ->select('users.firstname','users.lastname', 'group_users.group_name', 'users.group_users_id')
                    ->where('users.id','=',$userid)
                    ->get();
 
            return response()->json([
                'status' => true,
                'userid' => $userid,
                'userdata' => $user[0],
                'data' => $GroupMenu
            ], 200);
 
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
   
    public function updatePass(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(),
            [
                'newpassword' => 'required'
            ]);
 
            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
 
            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                $userakses = User::where('id', $userid)->first();
                $username = $userakses->user_name;
            }
 
            if($username == $request->newpassword){
                return response()->json([
                    'status' => false,
                    'message' => "password cant be same with username"
                ], 500);
            }
 
            User::where('id', $userid)
            ->update([
                'password' => Hash::make($request->newpassword)
            ]);
 
            return response()->json([
                'status' => true,
                'message' => 'Password Updated Successfully'
            ], 200);
 
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
   
    public function updateNewPass(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(),
            [
                'password' => 'required',
                'newpassword' => 'required'
            ]);
 
            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
 
            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                $userakses = User::where('id', $userid)->first();
                $username = $userakses->user_name;
            }
            // echo $request->user_name = $username;
            $datakirim = $request->only(['user_name', 'password']);
            $datakirim['user_name'] = $username;
            // print_r($datakirim);
            if(!Auth::attempt($datakirim)){
                return response()->json([
                    'status' => false,
                    'message' => 'Password does not match with our record.',
                ], 401);
            } else {
                User::where('id', $userid)
                ->update([
                    'password' => Hash::make($request->newpassword)
                ]);
               
                return response()->json([
                    'status' => true,
                    'message' => 'Password Updated Successfully'
                ], 200);
            }
 
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
 
    public function ForgotPass(Request $request){        
        try {
            $validateUser = Validator::make($request->all(),
            [
                'email' => 'required|email'
            ]);
 
            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
 
            $post_data = $request->all();
            $email = $post_data['email'];
            $user = User::where('email', $email)->first();
            // dd($user);
            if($user){
                $response = Mail::to($email)->send(new sendEmail($user));
                if($response)
                {
                    return response()->json([
                        'status' => true,
                        'norut' => $user['id'],
                        'message' => 'Email sent successfully.',
                    ]);
                }
                else
                {
                    return response()->json([
                        'status' => false,
                        'message' => 'Wrong Data'
                    ]);
                }
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Tidak Ditemukan'
                ], 404);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
   
    public function resetPass(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(),
            [
                'newpassword' => 'required'
            ]);
 
            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
 
            $post_data = $request->all();
            $userid = $post_data['norut']; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
 
 
            User::where('id', $userid)
            ->update([
                'password' => Hash::make($request->newpassword)
            ]);
 
            return response()->json([
                'status' => true,
                'message' => 'Password Updated Successfully'
            ], 200);
 
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}