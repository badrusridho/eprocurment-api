<?php

namespace App\Http\Controllers\Api;

use App\Models\GroupUser;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class GroupUserController extends Controller
{
    /**
     * Create GroupUser
     * @param Request $request
     * @return GroupUser
     * @return User 
     */
    public function createGroupUser(Request $request)
    {
        
        try {
            //Validated
            $validategroupUser = Validator::make($request->all(), 
            [
                'group_name' => 'required'
            ]);

            if($validategroupUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validategroupUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                $userakses = User::where('id', $userid)->first();
                $username = $userakses->user_name;
            } 

            $groupuser = GroupUser::create([
                'group_name' => $request->group_name,
                // 'create_by' => $request->create_by,
                'created_by' => $username
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Group User Created Successfully'
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Group User
     * @param Request $request
     * @return GroupUser
     */
    
     public function cboGroupUser(Request $request)
     {
         try {
             $groupuser = DB::table('group_users')
                     ->select('group_users.*')
                     ->where('group_users.deleted_by','=',null)
                     ->orderBy('id','asc')
                     ->get();
 
             return response()->json([
                 'data' => $groupuser
             ], 200);
         } catch (\Throwable $th) {
             return response()->json([
                 'status' => false,
                 'message' => $th->getMessage()
             ], 500);
         }
     }

    public function ViewGroupUser(Request $request)
    {
        $post_data = $request->all();
        $page = $post_data['page'];
        $rowpage = $post_data['rowpage'];
        $ordby = $post_data['ordby'];
        $ordasc = $post_data['ordasc'];

        $jumUser = DB::table('group_users')
        ->select('group_users.*')
        ->where('group_users.deleted_by','=',null)
        ->count();

        $jumdata = $jumUser;
        $jumpage = CEIL($jumUser/$rowpage);
        $start = ($page - 1) * $rowpage;

        try {
            $groupuser = DB::table('group_users')
                    ->select('group_users.*')
                    ->where('group_users.deleted_by','=',null)
                    ->skip($start)
                    ->take($rowpage)
                    ->orderBy($ordby,$ordasc)   
                    ->get();

            return response()->json([
                'data' => $groupuser,
                'jumData' => $jumdata,
                'jumPage' => $jumpage
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function searchGroupUser(Request $request)
    {
        $post_data = $request->all();
        try {
            // $groupuser = DB::table('group_users')
            //         ->select('group_users.*')
            //         ->where(function ($query) use ($post_data){
            //             // $query->where('group_users.group_name','like', '%'. $post_data['search'] .'%');
            //             $query->whereRaw("LOWER('group_users.group_name') LIKE '%'".strtolower($post_data['search'])."'%'");
            //         })
            //         ->where('group_users.deleted_by','=',null)
            //         ->get();
            $groupuser = DB::select("select * from group_users
                            where deleted_by is null
                            and LOWER(group_users.group_name) like '%".strtolower($post_data['search'])."%'");

            return response()->json([
                'status' => true,
                'data' => $groupuser
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    
    public function DeleteGroupUser(Request $request)
    {
        try {
            $datenow = date('Y-m-d H:i:s');
            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                $userakses = User::where('id', $userid)->first();
                $username = $userakses->user_name;
            } 

            $groupuser = GroupUser::where('id', $request->id)
            ->update([
                'deleted_by' => $username,
                'deleted_at' => $datenow
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Group User Deleted Successfully'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function updateGroupUser(Request $request)
    {
        try {
            //Validated
            $validategroupUser = Validator::make($request->all(), 
            [
                'group_name' => 'required'
            ]);

            if($validategroupUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validategroupUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                $userakses = User::where('id', $userid)->first();
                $username = $userakses->user_name;
            } 

            GroupUser::where('id', $request->id)
            ->update([
                'group_name' => $request->group_name,
                'updated_by' => $username
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Group User Updated Successfully'
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}