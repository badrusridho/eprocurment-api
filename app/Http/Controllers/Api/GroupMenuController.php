<?php

namespace App\Http\Controllers\Api;

use App\Models\GroupMenu;
use App\Models\Menu;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class GroupMenuController extends Controller
{
    /**
     * Create GroupMenu
     * @param Request $request
     * @return GroupMenu 
     * @return Menu 
     * @return User 
     */
    public function createGroupMenu(Request $request)
    {
        
        try {
            //Validated
            $validateGroupMenu = Validator::make($request->all(), 
            [
                'data' => 'required',
                'usergroup' => 'required'
            ]);

            if($validateGroupMenu->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateGroupMenu->errors()
                ], 401);
            }

            // $GroupMenu = GroupMenu::create([
            //     'group_name' => $request->group_name,
            //     // 'create_by' => $request->create_by,
            //     'created_by' => 'admin'
            // ]);
            
            $datenow = date('Y-m-d H:i:s');
            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                $userakses = User::where('id', $userid)->first();
                $username = $userakses->user_name;
            } 

            $data = $request->data;
            // foreach ($data as $row) {
            //     echo $row->level_menu;
            //     echo $row->menu_name;
            // }
            $jumdat=count($data);
            $deleted = GroupMenu::where('group_users_id', $request->usergroup)->delete();
            for($i = 0; $i < $jumdat; $i++)
            {
                // if($data[$i]['urlto']<>"#"){
                    $groupmenu = GroupMenu::create([
                        'group_users_id' => $request->usergroup,
                        'menu_id' => $data[$i]['menu_id'],
                        'created_by' => 'system',
                        'updated_by' => 'system',
                        'mview' => $data[$i]['mview'],
                        'madd' => $data[$i]['madd'],
                        'medit' => $data[$i]['medit'],
                        'mdelete' => $data[$i]['mdelete'],
                        'mapprove' => $data[$i]['mapprove'],
                        'mignore' => $data[$i]['mignore'],
                        'mexport' => $data[$i]['mexport'],
                        'created_by' => $username,
                        'updated_by' => $username
                    ]);
                // }
            }
            return response()->json([
                'status' => true,
                'message' => 'Group User Created Successfully'
                // 'message' => $data[0]['menu_name'].'-'.$jumdat
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Group User
     * @param Request $request
     * @return GroupMenu
     * @return Menu 
     */
    
    
    public function ViewGroupMenu(Request $request)
    {
        
        // return response()->json([
        //     'status' => false,
        //     'message' => 'Test'
        // ], 200);
        $grupid=$request->group_id;
        try {
            $GroupMenu = DB::table('menus')
                    // ->leftJoin('group_menus', 'menus.menu_id', '=', 'group_menus.menu_id', 'and', 'group_menus.group_users_id', '=', $request->group_id)
                    ->leftJoin('group_menus', function ($join) use ($grupid){
                        $join->on('menus.menu_id', '=', 'group_menus.menu_id')
                             ->where('group_menus.group_users_id', '=', $grupid);
                    })
                    ->select('menus.*', 'group_menus.group_users_id', 'group_menus.mview', 
                                'group_menus.madd', 'group_menus.medit', 'group_menus.mdelete', 
                                'group_menus.mapprove', 'group_menus.mignore', 'group_menus.mexport')
                    ->orderBy('menus.menu_seq')
                    ->get();

            // var_dump($GroupMenu);
            return response()->json([
                'data' => $GroupMenu,
                'message' => 'Test '.$grupid
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function ViewMenu(Request $request)
    {
        try {
            $Menu = Menu::where('deleted_by', '=' , null)->get();
            
            return response()->json([
                'data' => $Menu
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}