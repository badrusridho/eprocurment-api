<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class GroupMenu extends Authenticatable
{
    use HasFactory, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'group_users_id',
        'menu_id',
        'create_by',
        'created_at',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'mview',
        'madd',
        'medit',
        'mdelete',
        'mapprove',
        'mignore',
        'mexport'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
}
