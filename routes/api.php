<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\GroupUserController;
use App\Http\Controllers\Api\GroupMenuController;

// namespace App\Http\Controllers\API;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/auth/login', [UserController::class, 'loginUser']);

Route::post('/users/resetpass', [UserController::class, 'resetPass']);
Route::post('/users/forgotpass', [UserController::class, 'ForgotPass']);
Route::post('/users/updatepass', [UserController::class, 'updatePass']);
Route::post('/users/updatenewpass', [UserController::class, 'updateNewPass']);
Route::post('/users/register', [UserController::class, 'createUser']);
Route::post('/users/view', [UserController::class, 'ViewUser']);
Route::get('/users/count', [UserController::class, 'CountUser']);
Route::post('/users/update', [UserController::class, 'updateUser']);
Route::post('/users/search', [UserController::class, 'searchUser']);
Route::post('/users/delete', [UserController::class, 'DeleteUser']);
Route::post('/users/menu', [UserController::class, 'menuUser']);

Route::post('/groupusers/register', [GroupUserController::class, 'createGroupUser']);
Route::get('/groupusers/combo', [GroupUserController::class, 'cboGroupUser']);
Route::post('/groupusers/view', [GroupUserController::class, 'ViewGroupUser']);
Route::post('/groupusers/search', [GroupUserController::class, 'searchGroupUser']);
Route::post('/groupusers/update', [GroupUserController::class, 'updateGroupUser']);
Route::post('/groupusers/delete', [GroupUserController::class, 'DeleteGroupUser']);

Route::post('/groupmenu/view', [GroupMenuController::class, 'ViewGroupMenu']);
Route::get('/groupmenu/viewmenu', [GroupMenuController::class, 'ViewMenu']);
Route::post('/groupmenu/simpan', [GroupMenuController::class, 'createGroupMenu']);

